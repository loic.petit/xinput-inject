import time

import frida
import win32com.client
import win32gui
import win32process

from controller import *


def search_window(name):
    hwnd = win32gui.FindWindow(None, name)
    if hwnd is None:
        print("The window <%s> can't be found" % name)
        return None
    tid, pid = win32process.GetWindowThreadProcessId(hwnd)
    return pid, hwnd


class InputInject:
    def __init__(self, pid, hwnd, focus=False):
        self.pid = pid
        self.hwnd = hwnd
        self.script = None
        self.session = None
        self.needs_focus = focus
        self.queue = []

    def focus(self):
        win32com.client.Dispatch("WScript.Shell").SendKeys('%')
        win32gui.SetForegroundWindow(self.hwnd)
        # Wait a smidge for the focus to be effective
        time.sleep(0.05)

    def start(self):
        if self.session is not None:
            self.stop()
        try:
            self.session = frida.attach(self.pid)
            self.script = self.session.create_script(open("xinput_inject.js").read(), runtime="v8")
            self.focus()
            self.script.load()
        except Exception as e:
            self.session = None
            self.script = None
            print("Exception during startup...", e)
        self.queue = []

    def stop(self):
        if self.session is None:
            return
        try:
            self.script.unload()
            self.script = None
            self.session.detach()
            self.session = None
            print("Stopped xinput injection")
        except Exception as e:
            self.session = None
            self.script = None
            print("Exception during stopping...", e)

    def restart(self):
        self.stop()
        self.start()

    def send(self, player, sequence):
        self._send_inputs("xinput", player, sequence, as_xinput)

    def commit(self):
        if self.needs_focus:
            self.focus()
        self.script.post({"type": "queue", "payload": self.queue})
        self.queue = []

    def _send_inputs(self, message_type, player, sequence, transformer):
        if sequence is None or len(sequence) == 0:
            return
        sequence = [[command[0], transformer(command[1:])] for command in sequence]
        self.queue.append({
            "type": message_type,
            "payload": [player, sequence]
        })
