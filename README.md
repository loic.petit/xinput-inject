# XInput Injection

Tool to script XInput injection inside any game. It has been made for various street fighters but should
work for most games.

## Installation
You need to have a recent(-ish) python 3.7+

In order to be easier, during the installation of py3 select the box "Add python to environment variables".

Install the necessary libs using:
```
pip install frida-tools jupyter
```

## Run
Most of the execution part is done via a notebook.

Launch a command line in *administrator mode* (important). Go to the root of the project and do
```
jupyter lab
```

Then open [XInputInject.ipynb](XInputInject.ipynb), it contains the instructions to inject stuff in your game.

## Restrictions
The game might not directly like the injection and some restrictions may apply:
* Some games will require an actual XInput gamepad to be plugged in to inject inputs (SF6)
* Some games will require to have jupyter in admin mode to allow injection (SF5 notably)
* Some games will require focus to listen to inputs (SF4, SF5). See notes on that.