// WydD's XInput Laboratory
// Frida Injection script

// This contains the status of the injection
// when there is a two sized array, first element is player1, second is player2
var status = {
    nextXInput: [null, null],
    xmotionQueue: [[], []],
    // xpacketnumber is the packet number to override to be sure that the input is taken into account
    xpacketNumber: [Math.floor(1024 * Math.random()), Math.floor(1024 * Math.random())],
    duration: [0, 0],
    timing: [0, 0],
    queue: null

};

// utils
var nullPtr = ptr("0x0");
var readFloat = Memory.readFloat;
var readUInt = Memory.readUInt;
var readShort = Memory.readShort;
var readUShort = Memory.readUShort;
var readPointer = Memory.readPointer;
var readInt = Memory.readInt;
var readString = Memory.readAnsiString;
var readLong = Memory.readLong;

// "xinput" message:
// payload: [player_id, array_of_xinput_commands]
// xinput_command is [duration, [buttons, left_trigger, right_trigger]] (see later)
function evalXInput(value) {
    var player = value.payload[0];
    // If there is already a motion in progress, ignore the command
    if (status.xmotionQueue[player].length === 0) {
        var queue = value.payload[1];
        //queue.forEach(function(e) { e[0] *= 3; });
        if (queue.length > 1) {
            status.xmotionQueue[player] = queue;
            status.nextXInput[player] = null;
        } else {
            status.nextXInput[player] = queue[0];
        }
    }
    waitForXInput();
}

function waitForXInput() {
    recv('queue', function (value) {
        //console.log("Received inputs ", JSON.stringify(value.payload));
        status.queue = value.payload;
    });
}

/**
 * XInput Injection part!
 */

// write an xinput command into memory
// parameters:
//  * addr the memory pointer to write to
//  * towrite the xinput command
//  * packetNumber the packetNumber to use
// The xinput command is [buttons (uint16), left_trigger (uint8), right_trigger (uint8)]
// If you dont know how it works refer to https://msdn.microsoft.com/fr-fr/library/windows/desktop/microsoft.directx_sdk.reference.xinput_gamepad(v=vs.85).aspx
function writeXInput(addr, towrite, packetNumber) {
    var input = towrite[1];
    Memory.writeU32(addr, packetNumber);
    Memory.writeU16(addr.add(4), input[0]);
    Memory.writeU8(addr.add(6), input[1]);
    Memory.writeU8(addr.add(7), input[2]);
    towrite[0] -= 1;
    return towrite[0];
}

// XInput Interception via findExportByName on DLL
var XInputGetState = Module.findExportByName("XInput1_4.dll", "XInputGetState");
if (!XInputGetState)
    XInputGetState = Module.findExportByName("xinput1_3.dll", "XInputGetState");
if (!XInputGetState)
    XInputGetState = Module.findExportByName("xinput1_2.dll", "XInputGetState");
if (!XInputGetState)
    XInputGetState = Module.findExportByName("xinput1_1.dll", "XInputGetState");
if (!XInputGetState)
    XInputGetState = Module.findExportByName("XInput9_1_0.dll", "XInputGetState");
console.log("Sampling pollings per thread to select the right thread");

var countPerThread = {};
var selectedThread = 0;
var listener = Interceptor.attach(XInputGetState, {
    onEnter: function (args) {
        if (args[0].toInt32() !== 0) return;
        countPerThread[this.threadId] = (countPerThread[this.threadId] || 0) + 1;
    },
});
Thread.sleep(0.1);
listener.detach();
for (var threadId in countPerThread) {
    if (selectedThread === 0) selectedThread = parseInt(threadId);
    var c = countPerThread[threadId];
    if (c >= 5 && c <= 8) selectedThread = parseInt(threadId);
}
if (selectedThread === 0) {
    console.log("Error: XInput was never polled by the game!");
}
console.log("Picking the best thread amongst "+JSON.stringify(countPerThread)+". Selected: "+selectedThread);

// The idea is to overwrite the result of XInputGetState using args[0] (xinput id), and args[1] (the structure pointer)
// note: It needs to have an active xinput device for it to work as it overrides the device command
// I dont know how it works with direct input now
Interceptor.attach(XInputGetState, {
    onEnter: function (args) {
        this.ptr = args[1];
        this.n = args[0].toInt32();
    },
    onLeave: function (retval) {
        if (this.n >= 2) return;
        retval.replace(0);
        if (this.threadId !== selectedThread) return;
        var queue = status.xmotionQueue[this.n];
        var t = new Date().getTime();
        status.duration[this.n] = t - status.timing[this.n];
        status.timing[this.n] = t;
        if (queue != null && queue.length > 0) {
            if (writeXInput(this.ptr, queue[0], status.xpacketNumber[this.n]++) <= 0) {
                queue.shift();
            }
        } else if (status.nextXInput[this.n] != null) {
            if (writeXInput(this.ptr, status.nextXInput[this.n], status.xpacketNumber[this.n]++) <= 0) {
                status.nextXInput[this.n] = null;
            }
        }
        if (this.n === 0 && status.queue) {
            status.queue.forEach(function (content) {
                if (content.type === "xinput") {
                    evalXInput(content);
                }
            });
            status.queue = null;
        }
    }
});

// Listen to messages
waitForXInput();

// READY!
console.log("XInput Injection is operational");