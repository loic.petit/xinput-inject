P1 = 0
P2 = 1

XINPUT_DPAD_UP = 0x0001
XINPUT_DPAD_DOWN = 0x0002
XINPUT_DPAD_LEFT = 0x0004
XINPUT_DPAD_RIGHT = 0x0008
XINPUT_START = 0x0010
XINPUT_BACK = 0x0020
XINPUT_LEFT_THUMB = 0x0040
XINPUT_RIGHT_THUMB = 0x0080
XINPUT_LEFT_SHOULDER = 0x0100
XINPUT_RIGHT_SHOULDER = 0x0200
XINPUT_A = 0x1000
XINPUT_B = 0x2000
XINPUT_X = 0x4000
XINPUT_Y = 0x8000

XINPUT_LEFT_TRIGGER = -1
XINPUT_RIGHT_TRIGGER = -2

# Street Fighter Mapping
DOWN = XINPUT_DPAD_DOWN
UP = XINPUT_DPAD_UP
LEFT = XINPUT_DPAD_LEFT
RIGHT = XINPUT_DPAD_RIGHT
LP = XINPUT_X
MP = XINPUT_Y
HP = XINPUT_RIGHT_SHOULDER
LK = XINPUT_A
MK = XINPUT_B
HK = XINPUT_RIGHT_TRIGGER


# Transforms an input array to [button_mask, left_trigger, right_trigger]
def as_xinput(inputs):
    if inputs is None or len(inputs) == 0:
        return [0, 0, 0]
    buttons = 0
    left_trigger = 0
    right_trigger = 0

    for button in inputs:
        # Then transform into structure
        if button == -2:
            right_trigger = 200
        elif button == -1:
            left_trigger = 200
        else:
            buttons |= button
    return [buttons, left_trigger, right_trigger]

